/**
 * created on April 22nd 2019
 * Itexico Code Challenge
 */
package com.crud.itexico.CrudITexico.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crud.itexico.CrudITexico.model.User;
import com.crud.itexico.CrudITexico.service.UserService;


/**
 * The REST Webservice Class 
 * @author viruskimera
 *
 */
@RestController
public class UserController {
	
	/**
	 * The Service instance used
	 */
	private UserService service;
	
	/**
	 * To set the UserService instance
	 * @param inServ the spring injected service
	 */
	@Autowired
	public void setService(final UserService inServ){
		this.service = inServ;
	}
	
	/**
	 * To expose the REST get a User service
	 * @param id The id to retrieve user
	 * @return the User searched by the id 
	 */
	@GetMapping(value = "/{id}")
	public User getUser(@PathVariable final Long id){
		return service.getUserById(id);
	}
	
	/**
	 * To expose the REST AllUsers service
	 * @return list of ACTIVE users
	 */
	@GetMapping(value = "/")
	public List<User>getAllUsers(){
		return service.getAllUsers();
	}
	
	/**
	 * To expose the REST createNewUser service
	 * @param user the new USer values
	 * @return the user just created
	 */
	@PostMapping
	public ResponseEntity<User> createUser(@Valid @RequestBody User user){
		return ResponseEntity.ok(service.createUser(user));
	}
	
	/**
	 * To expose the REST deleteAUser service
	 * @param id
	 * @return the delete confirmation Message
	 */
	@DeleteMapping("/{id}")
	public String deleteUser(@PathVariable final Long id){
		 return service.deleteUser(id);
	}
	
	/**
	 * To expose the REST updateAUser service
	 * @param id the User id to be updated
	 * @param user the User with new values
	 * @return the updated User
	 */
	@PutMapping(value = "/{id}")
	public User updateUser(@PathVariable final Long id, @RequestBody User user){
		return service.updateUser(id, user);
	}
}
