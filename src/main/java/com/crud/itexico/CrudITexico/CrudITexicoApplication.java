package com.crud.itexico.CrudITexico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudITexicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudITexicoApplication.class, args);
	}

}
