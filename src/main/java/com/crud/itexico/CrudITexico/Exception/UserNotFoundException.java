/**
 * 
 */
package com.crud.itexico.CrudITexico.Exception;

/**
 * @author viruskimera
 *
 */
public class UserNotFoundException extends RuntimeException{

	/**
	 * Basic Custom Exception  for not User with particular Id
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * THe constructor
	 * @param id the User id
	 */
	public UserNotFoundException(final Long id){
		super("No User with the id: " + id);
	}

}
