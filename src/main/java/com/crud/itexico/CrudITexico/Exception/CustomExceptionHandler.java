/**
 * 
 */
package com.crud.itexico.CrudITexico.Exception;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author viruskimera
 *
 */
@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	
	/**
	 * Just ot change the HTTP status Code to 404
	 * @param response
	 * @throws IOException
	 */
    @ExceptionHandler(UserNotFoundException.class)
    public void springHandleNotFound(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }
    
    /**
     * To solve JACKSON not errors shown
     */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
        Exception ex, Object body, HttpHeaders headers, HttpStatus status, 
        WebRequest request) {
    	return new ResponseEntity<>(
    		  getBody(HttpStatus.BAD_REQUEST, ex, "Something Went Wrong"), 
    		  headers, 
    		  status);
    }
    
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exception(Exception ex) {
        return new ResponseEntity<>(
        		getBody(HttpStatus.INTERNAL_SERVER_ERROR, ex, "Something Else Went Wrong"), 
        		new HttpHeaders(), 
        		HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    /**
     * To create a custom ResponseEntity
     * @param status
     * @param ex
     * @param message
     * @return
     */
    public Map<String, Object> getBody(HttpStatus status, Exception ex, String message) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", message);
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("error", status.getReasonPhrase());
        body.put("detailed Exception", ex.toString());

        Throwable cause = ex.getCause();
        if (cause != null) {
            body.put("exceptionCause", ex.getCause().toString());
        }
        return body;
    }
}