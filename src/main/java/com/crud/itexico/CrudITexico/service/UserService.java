/**
 * 
 */
package com.crud.itexico.CrudITexico.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.crud.itexico.CrudITexico.dao.UserDAO;
import com.crud.itexico.CrudITexico.model.User;

/**
 * @author viruskimera
 *
 */
public interface UserService {
	
	/**
	 * To get list of active Users from DB
	 * @return Active Users
	 */
	public List<User> getAllUsers();
	
	/**
	 * To insert a new User in DB
	 * @param user
	 * @return
	 */
	public User createUser(final User user);
	
	/**
	 * To get the USer searched by the id provided
	 * @param id
	 * @return
	 */
	public User getUserById(final Long id);
	
	/**
	 * To delete a User
	 * <p>The user status is set to INACTIVE
	 * @param id
	 * @return
	 */
	public String deleteUser(final Long id);

	/**
	 * To update a User`s basic info:
	 * <p>FirstName, LastNAme and DateOfBirth
	 * @param id
	 * @param user
	 * @return
	 */
	public User updateUser(final Long id, final User user);

}
