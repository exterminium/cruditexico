/**
 * 
 */
package com.crud.itexico.CrudITexico.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crud.itexico.CrudITexico.dao.UserDAO;
import com.crud.itexico.CrudITexico.model.User;

/**
 * @author viruskimera
 *
 */
@Service
public class UserServiceImpl implements UserService{

	/**
	 * The DAO instance
	 */
	private UserDAO userDAO;
	
	/**
	 * @param userDAO the userDAO to set
	 */
	@Autowired
	public void setUserDAO(UserDAO inUserDAO) {
		this.userDAO = inUserDAO;
	}

	@Override
	public List<User> getAllUsers(){
		return userDAO.findByStatus("ACTIVE");
	}
	
	public User createUser(final User user){
		return userDAO.save(user);
	}
	
	public User getUserById(final Long id){
		return userDAO.getUserById(id);
	}
	
	@Override
	public String deleteUser(final Long id){
		return userDAO.deleteUser(id);
	}

	@Override
	public User updateUser(Long id, User user) {
		return userDAO.updateUser(id, user);
	}
}
