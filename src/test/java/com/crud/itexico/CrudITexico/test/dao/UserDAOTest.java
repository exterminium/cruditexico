/**
 * 
 */
package com.crud.itexico.CrudITexico.test.dao;

import java.sql.Date;

import org.junit.Before;

import com.crud.itexico.CrudITexico.dao.UserDAO;
import com.crud.itexico.CrudITexico.model.User;

/**
 * @author viruskimera
 *
 */
public class UserDAOTest {
	
	private UserDAO dao;
	
	/**
	 * The setup method
	 */
	@Before
	public void setup(){
	}
	
	public void successCase(){
		
	}
	
	/**
	 * To set the User
	 * @param validUsr
	 */
	private void setValidUser(User validUsr){
		validUsr.setId(new Long(1));
		validUsr.setFirstName("testFirstName");
		validUsr.setLastName("testLastName");
		validUsr.setDateOfBirth(new Date(0));
	}

}
